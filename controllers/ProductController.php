<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\filters\AccessControl;
use \yii\web\Controller;

class ProductController extends Controller
{

    public function actionTest()
    {
        $products = Yii::$app->db->createCommand('SELECT * FROM products')->queryAll();
        Yii::$app->response->format = Response::FORMAT_JSON;                                                  // формат json
        Yii::$app->response->data = $products;
    }

    public function actionTestFin()
    {
        $products = "test";
        Yii::$app->response->format = Response::FORMAT_JSON;                                                  // формат json
        Yii::$app->response->data = $products;
    }

    public function actionGetProductList()
    {
        $products = Yii::$app->db->createCommand('SELECT * FROM products')->queryAll();
        Yii::$app->response->format = Response::FORMAT_JSON;                                                  // формат json
        Yii::$app->response->data = $products;
    }

}